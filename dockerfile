FROM node:7
WORKDIR /var/lib/jenkins/workspace/docker/
COPY . /var/lib/jenkins/workspace/docker/
RUN npm install
CMD node index.js
EXPOSE 8081
